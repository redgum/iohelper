﻿Imports System.IO
Imports System.Security.AccessControl

Public Class PermissionHelper
    ''' <summary>
    ''' Returns a message based on the status of the path
    ''' </summary>
    ''' <param name="path"></param>
    ''' <returns></returns>
    ''' <remarks>Checks: Path existence,Can read,Can write,Can delete,Can create sub directories</remarks>
    Public Shared Function GetPathStatus(path As String) As String
        'Write a file
        Try
            If Not IO.Directory.Exists(path) Then Return "Path does not exist"
            If Not HasFileReadPermissionOnDirectory(path) Then Return "Does not have sufficient permission to read files from path"
            If Not HasFileWritePermissionOnDirectory(path) Then Return "Does not have sufficient permission to write files to path"
            If Not HasFileDeletePermissionOnDirectory(path) Then Return "Does not have sufficient permission to delete files in path"
            If Not HasDirectoryCreatePermissionOnDirectory(path) Then Return "Does not have sufficient permission to create sub directories in path"

        Catch ex As Exception
            Return ex.GetBaseException.Message
        End Try

        Return "OK"
    End Function

    Public Shared Function HasFileWritePermissionOnDirectory(path As String) As Boolean
        Dim writeAllow = False
        Dim writeDeny = False
        Dim accessControlList = GetAccessControlList(path)
        If accessControlList Is Nothing Then
            Return False
        End If
        Dim accessRules = accessControlList.GetAccessRules(True, True, GetType(System.Security.Principal.SecurityIdentifier))
        If accessRules Is Nothing Then
            Return False
        End If

        For Each rule As FileSystemAccessRule In accessRules
            If (FileSystemRights.Write And rule.FileSystemRights) <> FileSystemRights.Write Then
                Continue For
            End If

            If rule.AccessControlType = AccessControlType.Allow Then
                writeAllow = True
            ElseIf rule.AccessControlType = AccessControlType.Deny Then
                writeDeny = True
            End If
        Next

        Return writeAllow AndAlso Not writeDeny
    End Function

    Public Shared Function HasDirectoryCreatePermissionOnDirectory(path As String) As Boolean
        Dim writeAllow = False
        Dim writeDeny = False
        'walk up to the first directory that exists
        Dim lpath = FindParentDirectoryExists(path)
        'can only test something that exists!
        Dim accessControlList = GetAccessControlList(lpath)
        If accessControlList Is Nothing Then
            Return False
        End If
        Dim accessRules = accessControlList.GetAccessRules(True, True, GetType(System.Security.Principal.SecurityIdentifier))
        If accessRules Is Nothing Then
            Return False
        End If

        For Each rule As FileSystemAccessRule In accessRules
            If (FileSystemRights.CreateDirectories And rule.FileSystemRights) <> FileSystemRights.CreateDirectories Then
                Continue For
            End If

            If rule.AccessControlType = AccessControlType.Allow Then
                writeAllow = True
            ElseIf rule.AccessControlType = AccessControlType.Deny Then
                writeDeny = True
            End If
        Next

        Return writeAllow AndAlso Not writeDeny
    End Function

    Public Shared Function HasFileDeletePermissionOnDirectory(path As String) As Boolean
        Dim writeAllow = False
        Dim writeDeny = False
        Dim accessControlList = GetAccessControlList(path)
        If accessControlList Is Nothing Then
            Return False
        End If
        Dim accessRules = accessControlList.GetAccessRules(True, True, GetType(System.Security.Principal.SecurityIdentifier))
        If accessRules Is Nothing Then
            Return False
        End If

        For Each rule As FileSystemAccessRule In accessRules
            If (FileSystemRights.Delete And rule.FileSystemRights) <> FileSystemRights.Delete Then
                Continue For
            End If

            If rule.AccessControlType = AccessControlType.Allow Then
                writeAllow = True
            ElseIf rule.AccessControlType = AccessControlType.Deny Then
                writeDeny = True
            End If
        Next

        Return writeAllow AndAlso Not writeDeny
    End Function

    Public Shared Function HasFileReadPermissionOnDirectory(path As String) As Boolean
        Dim writeAllow = False
        Dim writeDeny = False
        Dim accessControlList = GetAccessControlList(path)
        If accessControlList Is Nothing Then
            Return False
        End If
        Dim accessRules = accessControlList.GetAccessRules(True, True, GetType(System.Security.Principal.SecurityIdentifier))
        If accessRules Is Nothing Then
            Return False
        End If

        For Each rule As FileSystemAccessRule In accessRules
            If (FileSystemRights.Read And rule.FileSystemRights) <> FileSystemRights.Read Then
                Continue For
            End If

            If rule.AccessControlType = AccessControlType.Allow Then
                writeAllow = True
            ElseIf rule.AccessControlType = AccessControlType.Deny Then
                writeDeny = True
            End If
        Next

        Return writeAllow AndAlso Not writeDeny
    End Function

    Private Shared Function GetAccessControlList(Path As String) As DirectorySecurity
        Dim accessControlList As DirectorySecurity
        Try
            accessControlList = Directory.GetAccessControl(Path)
        Catch ex As Exception
            'if we errored here, we have failed...
            Return Nothing
        End Try
        Return accessControlList
    End Function
    Private Shared Function FindParentDirectoryExists(Path As String) As String
        If IO.Directory.Exists(Path) Then
            Return Path
        Else
            Dim lparentpath = IO.Directory.GetParent(Path).FullName
            Return FindParentDirectoryExists(lparentpath)
        End If
    End Function

    Public Shared Function HasReadPermissionOnFile(fileName As String) As Boolean
        Dim writeAllow = False
        Dim writeDeny = False
        Dim accessControlList = File.GetAccessControl(fileName)
        If accessControlList Is Nothing Then
            Return False
        End If
        Dim accessRules = accessControlList.GetAccessRules(True, True, GetType(System.Security.Principal.SecurityIdentifier))
        If accessRules Is Nothing Then
            Return False
        End If

        For Each rule As FileSystemAccessRule In accessRules
            If (FileSystemRights.Read And rule.FileSystemRights) <> FileSystemRights.Read Then
                Continue For
            End If

            If rule.AccessControlType = AccessControlType.Allow Then
                writeAllow = True
            ElseIf rule.AccessControlType = AccessControlType.Deny Then
                writeDeny = True
            End If
        Next

        Return writeAllow AndAlso Not writeDeny
    End Function
End Class
