﻿Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Imports System.Text.RegularExpressions

Public Class FileHelper
    Public Const MaxFileNameLength As Integer = 260

    ''' <summary>
    ''' Checks to see if a given filename is a valid windows filename.
    ''' </summary>
    ''' <param name="fileName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function IsValidFileName(fileName As String) As Boolean
        'Note: This is not the full set of goodness...
        'for more details - http://msdn.microsoft.com/en-us/library/aa365247%28v=vs.85%29.aspx#file_and_directory_names
        ' and http://stackoverflow.com/questions/62771/how-check-if-given-string-is-legal-allowed-file-name-under-windows/62888#62888

        'check for empty filename
        If String.IsNullOrWhiteSpace(fileName) Then
            Return False
        End If

        'check for filename is too long
        If fileName.Length > MaxFileNameLength Then
            Return False
        End If

        'check for restricted filenames - Only the filename Part...
        Dim lReservedFileNames = {"CON", "PRN", "AUX", "NUL", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"}
        If lReservedFileNames.Contains(IO.Path.GetFileNameWithoutExtension(fileName)) Then
            Return False
        End If

        'check for all dots "...."
        Dim AllDots = New Regex("^[\.]+$")
        If AllDots.IsMatch(fileName) Then
            Return False
        End If

        'check to see if there are any invalid characters in the filename
        Dim containsABadCharacter = New Regex("[" + Regex.Escape(Path.GetInvalidFileNameChars) + "]")
        If containsABadCharacter.IsMatch(fileName) Then Return False

        Return True
    End Function

    Public Enum FileNameWhitespaceOptions
        PreserveSpaces
        ConvertToDashes
        ConvertToUnderscores
        RemoveSpaces
    End Enum

    Public Enum FilenameHandlingOptions
        TreatAsPath
        TreatAsFilename
    End Enum

    Public Shared Function CreateValidFileName(filename As String) As String
        Return CreateValidFileName(filename, FileNameWhitespaceOptions.PreserveSpaces)
    End Function

    Public Shared Function CreateValidFileName(filename As String, filenameHandlingOptions As FilenameHandlingOptions) As String
        Return CreateValidFileName(filename, FileNameWhitespaceOptions.PreserveSpaces, filenameHandlingOptions)
    End Function

    Public Shared Function CreateValidFileName(filename As String, spaceHandlingOptions As FileNameWhitespaceOptions) As String
        Return CreateValidFileName(filename, spaceHandlingOptions, FilenameHandlingOptions.TreatAsFilename)
    End Function

    Public Shared Function CreateValidFileName(filename As String, spaceHandlingOptions As FileNameWhitespaceOptions, filenameHandlingOptions As FilenameHandlingOptions) As String
        Dim result As String = String.Empty
        Dim lFileName As String
        Dim lFileExtension As String

        Dim containsABadCharacter = New Regex("[" + Regex.Escape(Path.GetInvalidFileNameChars) + "]")

        If filenameHandlingOptions = FileHelper.FilenameHandlingOptions.TreatAsFilename Then
            'remove all the slashes...
            Dim StrippedFilename = filename.Replace("\", "").Replace("/", "").Replace(":", "")
            lFileName = Path.GetFileNameWithoutExtension(StrippedFilename)
            lFileExtension = Path.GetExtension(StrippedFilename)

        Else
            lFileName = Path.GetFileNameWithoutExtension(filename)
            lFileExtension = Path.GetExtension(filename)
        End If

        'remove all bad characters and trim
        lFileName = containsABadCharacter.Replace(lFileName, String.Empty).Trim

        'handle Whitespace based on options
        Select Case spaceHandlingOptions
            Case FileNameWhitespaceOptions.RemoveSpaces
                lFileName = lFileName.Replace(" ", "")
                lFileExtension = lFileExtension.Replace(" ", "")
            Case FileNameWhitespaceOptions.ConvertToUnderscores
                lFileName = lFileName.Replace(" ", "_")
                lFileExtension = lFileExtension.Replace(" ", "_")
            Case FileNameWhitespaceOptions.ConvertToDashes
                lFileName = lFileName.Replace(" ", "-")
                lFileExtension = lFileExtension.Replace(" ", "-")
            Case Else
                'default case - preseve spaces and so do nothing...
        End Select

        'assemble final filename
        result = String.Format("{0}{1}", lFileName, lFileExtension)

        'If our resulting filename is too long, then chop down the filename part...
        If result.Length > MaxFileNameLength Then
            result = String.Format("{0}{1}", Left(lFileName, MaxFileNameLength - lFileExtension.Length), lFileExtension)
        End If

        If Not IsValidFileName(result) Then
            Throw New UnableToCreateValidFilenameException
        End If
        Return result
    End Function
#Region "Hashing"
    Public Shared Function GetFileContentsHash(filePath As String) As String
        Using fs As FileStream = New FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read)
            Return GetFileContentsHash(fs)
        End Using
    End Function

    Public Shared Function GetFileContentsHash(stream As FileStream) As String
        Dim sb As StringBuilder = New StringBuilder()

        If stream Is Nothing Then Return Nothing
        If stream.Position <> 0 AndAlso Not stream.CanSeek Then Return Nothing

        Dim lPrevPosition As Long = stream.Position
        stream.Seek(0, SeekOrigin.Begin)

        Using md5 As MD5 = MD5CryptoServiceProvider.Create()
            Dim hash As Byte() = md5.ComputeHash(stream)
            For Each b As Byte In hash
                sb.Append(b.ToString("x2"))
            Next
        End Using

        stream.Seek(lPrevPosition, SeekOrigin.Begin)

        Return sb.ToString()
    End Function
#End Region

End Class
