﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()>
Public Class IsValidFileNameTest

    <TestMethod()>
    Public Sub GoodFileNameSimple()
        Dim lFilename = "aGoodFileName.txt"
        Assert.IsTrue(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub GoodFileNameComplex()
        Dim lFilename = "a.Good.File.Name.txt"
        Assert.IsTrue(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub GoodFileNameSpaces()
        Dim lFilename = "a Good File Name.txt"
        Assert.IsTrue(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub GoodFileNameLongExtension()
        Dim lFilename = "a Good File Name.document"
        Assert.IsTrue(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub GoodFileNameNoExtension()
        Dim lFilename = "aGoodFileName"
        Assert.IsTrue(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub BadFileNameSimple()
        Dim lFilename = "aBad$*.FileName"
        Assert.IsFalse(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub BadFileNameReserved()
        Dim lFilename = "CON"
        Assert.IsFalse(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub BadFileNameReserved2()
        Dim lFilename = "CON.txt"
        Assert.IsFalse(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub BadFileNameLong()
        Dim lFilename = "ThisIsAVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLong.FileName"
        Assert.IsFalse(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub BadFileNameAllPeriods()
        Dim lFilename = "....."
        Assert.IsFalse(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub BadFileNamePathSeparator()
        Dim lFilename = "a Bad\ File Name.txt"
        Assert.IsFalse(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub BadFileNamePathSeparatorUnix()
        Dim lFilename = "a Bad/File Name.txt"
        Assert.IsFalse(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub BadFileNameWhitespace()
        Dim lFilename = "  "
        Assert.IsFalse(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub BadFileNameNull()
        Dim lFilename As String
        Assert.IsFalse(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub BadFileNameEmptyString()
        Dim lFilename = String.Empty
        Assert.IsFalse(Redgum.Components.IOHelper.FileHelper.IsValidFileName(lFilename))
    End Sub

End Class