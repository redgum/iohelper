﻿<TestClass()>
Public Class CreateValidFileNameTest

    <TestMethod()>
    Public Sub GoodFileNameSimple()
        Dim lFilename = "aGoodFileName.txt"
        Assert.AreEqual(lFilename, Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lFilename))
    End Sub

    <TestMethod()>
    Public Sub GoodFileNameSpaces()
        Dim lFilename = "a Good File Name.txt"
        Assert.AreEqual(lFilename, Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lFilename))
    End Sub
    <TestMethod()>
    Public Sub GoodFileNameSpacesPreserveSpaces()
        Dim lFilename = "a Good File Name.txt"
        Assert.AreEqual(lFilename, Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lFilename, FileHelper.FileNameWhitespaceOptions.PreserveSpaces))
    End Sub
    <TestMethod()>
    Public Sub GoodFileNameSpacesConvertedToUnderscores()
        Dim lInputFilename = "a Good File Name.t xt"
        Dim lResultFileName = "a_Good_File_Name.t_xt"
        Assert.AreEqual(lResultFileName, Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lInputFilename, FileHelper.FileNameWhitespaceOptions.ConvertToUnderscores))
    End Sub
    <TestMethod()>
    Public Sub GoodFileNameSpacesConvertedToDashes()
        Dim lInputFilename = "a Good File Name.txt"
        Dim lResultFileName = "a-Good-File-Name.txt"
        Assert.AreEqual(lResultFileName, Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lInputFilename, FileHelper.FileNameWhitespaceOptions.ConvertToDashes))
    End Sub
    <TestMethod()>
    Public Sub GoodFileNameSpacesRemoved()
        Dim lInputFilename = "a Good File Name.txt"
        Dim lResultFileName = "aGoodFileName.txt"
        Assert.AreEqual(lResultFileName, Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lInputFilename, FileHelper.FileNameWhitespaceOptions.RemoveSpaces))
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(Redgum.Components.IOHelper.UnableToCreateValidFilenameException))>
    Public Sub BadFileNameDisappear()
        Dim lFilename = "**"

        Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lFilename)

    End Sub

    <TestMethod()>
    Public Sub BadFileNameConvertedColons()
        Dim lInputFilename = "Blah Blah : \\ blah.csv"

        Dim lResultFileName = "Blah Blah   blah.csv"
        Assert.AreEqual(lResultFileName, Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lInputFilename, FileHelper.FilenameHandlingOptions.TreatAsFilename))
    End Sub

    <TestMethod()>
    Public Sub BadFileNameConvertedAsFileName()
        Dim lInputFilename = "Bad/File\Name.txt"

        Dim lResultFileName = "BadFileName.txt"
        Assert.AreEqual(lResultFileName, Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lInputFilename, FileHelper.FilenameHandlingOptions.TreatAsFilename))
    End Sub

    <TestMethod()>
    Public Sub BadFileNameConvertedAsPath()
        Dim lInputFilename = "Bad\File/Name.txt"

        Dim lResultFileName = "Name.txt"
        Assert.AreEqual(lResultFileName, Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lInputFilename, FileHelper.FilenameHandlingOptions.TreatAsPath))
    End Sub

    <TestMethod()>
    Public Sub BadFileNameLong()
        Dim lInputFilename = "ThisIsAVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLong.FileName"
        Dim lResultFileName = "ThisIsAVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVery.FileName"
        Assert.AreEqual(lResultFileName, Redgum.Components.IOHelper.FileHelper.CreateValidFileName(lInputFilename))
    End Sub
End Class
