# Redgum.Components.IOHelper

The IOHelper library contains a set of extension methods designed to wrap the ACL calls to determine if the current user can read/write/delete a file or folder.

**Nuget**: [https://www.nuget.org/packages/Redgum.Components.IOHelper/](https://www.nuget.org/packages/Redgum.Components.IOHelper/)

### License: MIT

